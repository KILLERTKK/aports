# Contributor: psykose <alice@ayaya.dev>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=stylua
pkgver=0.13.0
pkgrel=0
pkgdesc="Opinionated Lua 5.1/5.2/luau code formatter"
url="https://github.com/JohnnyMorganz/StyLua"
arch="all !s390x !riscv64" # blocked by cargo
license="MPL-2.0"
makedepends="cargo"
source="$pkgname-$pkgver.tar.gz::https://github.com/JohnnyMorganz/StyLua/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/StyLua-$pkgver"

prepare() {
	default_prepare
	cargo fetch --locked
}

build() {
	cat >> Cargo.toml <<- EOF
		[profile.release]
		codegen-units = 1
		lto = true
		opt-level = "s"
		panic = "abort"
	EOF
	cargo build --release --frozen
}

check() {
	cargo test --frozen
}

package() {
	install -Dm755 "$builddir"/target/release/stylua \
		-t "$pkgdir"/usr/bin
}

sha512sums="
87e94805a5ba8e31a692fa6edf5f025462ea4eee2c18d194ae369d2e953e915a8129b0aecda22467616e6a40d74ffa2bd633684fe781918da9ccf2c078d70de1  stylua-0.13.0.tar.gz
"
