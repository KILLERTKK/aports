# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=difftastic
pkgver=0.25.0
pkgrel=0
pkgdesc="Diff tool that understands syntax"
url="https://difftastic.wilfred.me.uk/"
license="MIT"
arch="all !s390x !riscv64" # blocked by rust/cargo
arch="$arch !aarch64 !armhf !armv7" # FTBFS on arm
makedepends="cargo openssl-dev"
source="https://github.com/Wilfred/difftastic/archive/$pkgver/difftastic-$pkgver.tar.gz"

prepare() {
	default_prepare
	cargo fetch --locked
}

build() {
	cargo build --frozen --release
}

check() {
	cargo test --frozen
}

package() {
	install -Dm755 target/release/difft -t "$pkgdir"/usr/bin
}

sha512sums="
b421a591cbd896cacff832ce0e39cb9fc952690f2e334969bdd2722ff17b43f5c6fd81a4223490e38e0a9c5b639a558c6c3682123d5280ba5f29950072626473  difftastic-0.25.0.tar.gz
"
